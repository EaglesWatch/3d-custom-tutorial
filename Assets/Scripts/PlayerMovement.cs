﻿
using UnityEngine;

public class PlayerMovementLogic : MonoBehaviour {

    public Rigidbody rb;
    public float forwardForce = 2000f;
    public float sideForce = 50f;
    public float max_side_speed = 15f;

	// Use this for initialization
	void Start () {
        
        





	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //add a forward force
        rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        if (Input.GetKey("d"))
        {
            rb.AddForce(sideForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);

        }
        if (Input.GetKey("a"))
        {
            rb.AddForce(-sideForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);

        }
        rb.velocity = new Vector3(
           Mathf.Clamp(rb.velocity.x, -max_side_speed, max_side_speed),
           rb.velocity.y, rb.velocity.z
           );

        if (rb.position.y < -1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}
