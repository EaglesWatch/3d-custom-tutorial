﻿using UnityEngine.SceneManagement;

using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject completeLevelUI;

    bool gameHasEnded = false;

    public void CompleteLevel ()
    {
        completeLevelUI.SetActive(true);
    }

    public void EndGame ()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("Game Over");
            Invoke("Restart", 1f);
        }
    }
	void Restart ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
